#!/bin/bash
# Watchdog Script for CrazyCraft
# Written by Jesse N. Richardson (jr.fire.flare@gmail.com) [negativeflare]

# Sudocode:
# Write out forge tps out to the log
# If the log on the server reads 'Overall:' at the end of the log,
#  then it'll push a message about watchdog to the server using say
#  if doesn't get anything back, it'll try to kill the server gracefully
#  if that doesn't work, it'll kill the tmux session, wait a bit then kill java
#  then it'll spin the server back up.

# First Thing I need to do is get the date and time in the correct format from the server
# Correct Format: '[05Apr2023 09:26:51.070]'

#current_date=$(date +"%d%a%Y %H:%M")

# Let's poke the server
tmux send-keys -t crazycraft forge\ tps C-m

# Grep out the output
output=$(tail /mnt/WDBlueSSD/Minecraft/crazycraft/logs/latest.log |  grep -o 'Overall: Mean tick time:')

if [[ $output = "Overall: Mean tick time:" ]]; then
    echo "$output"
    echo "Server is running!"
else
        tmux send-keys -t crazycraft stop C-m
        sleep 5
        tmux kill-window -t crazycraft
        sleep 5
        killall -9 java
        sleep 5
        killall -9 java
        echo "Restarting Server.."
        cd /mnt/WDBlueSSD/Minecraft/crazycraft/
        tmux new -d -s crazycraft /usr/lib/jvm/java-8-openjdk-amd64/bin/java -Xms16G -Xmx16G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=20 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=32 -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Dsun.rmi.dgc.server.gcInterval=600000 -Daikars.new.flags=true -Dlog4j2.formatMsgNoLookups=true -jar forge.jar nogui
        sleep 300
        tmux send-keys -t crazycraft say\ MC\ WATCHDOG\ HAS\ DETECTED\ A\ CRASH! C-m
        sleep 2
        tmux send-keys -t crazycraft say\ MC\ WATCHDOG\ HAS\ RESTARTED\ THE\ SERVER! C-m
    fi
fi
