#!/bin/bash
# Server Restart Script
# Written by Jesse N. Richardson (jr.fire.flare@gmail.com) [negativeflare]
# Written way too dang late into the night.

tmux send-keys -t crazycraft stop C-m
sleep 5
tmux kill-window
sleep 5
killall -9 java
cd /home/minecraft/crazycraft/
sudo --user=minecraft "tmux new -d -s crazycraft /usr/lib/jvm/java-8-openjdk-amd64/bin/java -Xms16G -Xmx16G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=20 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=32 -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Dsun.rmi.dgc.server.gcInterval=600000 -Daikars.new.flags=true -Dlog4j2.formatMsgNoLookups=true -jar forge.jar nogui"
