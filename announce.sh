#!/bin/bash
# CC Announce Script
# Written by Jesse N. Richardson (jr.fire.flare@gmail.com) [negativeflare]

# This script prints out some reminders then waits 30 minutes then does another one.

# 1 Hour
tmux send-keys -t crazycraft say\ Remember!\ Always\ let\ everyone\ know\ when\ you\ are\ using\ RTP! C-m
sleep 900
tmux send-keys -t crazycraft say\ Alfeim
tmux send-keys -t crazycraft say\ Remember!\ You\ can\ only\ have\ 3\ Clocks\ of\ Flowing\ Time! C-m
sleep 1800
# ---

# 1 Hour
tmux send-keys -t crazycraft say\ Remember!\ 10\ Power\ Flowers\ Per\ Player! C-m
sleep 1800
tmux send-keys -t crazycraft say\ If\ you\ think\ it\ will\ lag\ the\ server,\ then\ stop! C-m
sleep 1800
# ---

# 1 Hour
tmux send-keys -t crazycraft say\ Be\ nice\ to\ others! C-m
sleep 1800
tmux send-keys -t crazycraft say\ Remember\ to\ claim\ your\ land! C-m
sleep 1800
# ---

# 1 Hour
tmux send-keys -t crazycraft say\ If\ the\ Server\ Crashes\ please\ let\ us\ know\ by\ doing\ @OP\ in\ Discord C-m
sleep 1800
tmux send-keys -t crazycraft say\ Do\ NOT\ Use\ The\ Pandoras\ Box\ Mod\ as\ it\ is\ bugged C-m
sleep 1800
# --

# 1 Hour
tmux send-keys -t crazycraft say\ Use\ The\ Tardis\ At\ Your\ Own\ Risk.\ Do\ Not\ Log\ Out\ In\ a\ Tardis! C-m
sleep 1800
tmux send-keys -t crazycraft say\ If\ You\ Grief\ You\ Get\ Banned\ Period. C-m
sleep 1800
# --
