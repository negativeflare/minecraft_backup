#!/bin/bash
# CC Restart Script

echo "Informing players to disconnect"
tmux send-keys -t crazycraft say\ Restarting\ Server C-m
sleep 5
tmux send-keys -t crazycraft say\ Please\ Disconnect C-m
sleep 10

echo "Saving the Game and Shutting Down the Server"
tmux send-keys -t crazycraft save-all
sleep 30
tmux send-keys -t crazycraft stop
sleep 200
killall -9 java


sleep 10
echo "Firing up CC Server"
cd /mnt/Minecraft/crazycraft/
tmux new -d -s crazycraft /usr/lib/jvm/java-8-openjdk-amd64/bin/java -Xms15G -Xmx15G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=20 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=32 -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Dsun.rmi.dgc.server.gcInterval=600000 -Daikars.new.flags=true -Dlog4j2.formatMsgNoLookups=true -jar cc-forge.jar nogui
sleep 120
tmux send-keys -t crazycraft say\ Server\ Restart\ Complete C-m
